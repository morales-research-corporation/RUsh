# Research Unix Shell (discontinued as of Jan 2022)
A shell written in Rust and compatiable with all Linux distros and operating systems

## Building and executing the shell
Give the program octal permissions (You have to be in the root directory of the folder)

```chmod +x target/release/Research-Unix-Shell```

And then execute...

```./Research-Unix-Shell```

(C) 2021 Morales Research Corporation

This project has been discontinued as of January 31, 2022 and will now be an archived project as of June 28, 2023
